CSView is a minimal table-style viewer for CSV files. It uses very little memory and is able to display the start of the file immediately, even for very large (multi-GB) files. It can comfortably open files larger than 4GB.

Compatible with Windows, MacOS and Linux.

Packaged builds are available from https://kothar.net/csview

![Screenshot](screenshots/MacOS%20Mojave_1440.jpg)

Icon by [Honza Dousek](http://jandousek.cz)
([See also](https://www.iconfinder.com/iconsets/lexter-flat-colorfull-file-formats))