# Privacy Policy

CSView does not collect any personal data or send any data to remote servers.

Anonymous download metrics are collected through the Mac App Store 
and the Bitbucket Downloads page.

Google Analytics is used to track interaction with the Bitbucket project.

Metrics are not personally identifiable and are not shared with any third parties. 