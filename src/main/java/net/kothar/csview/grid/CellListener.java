package net.kothar.csview.grid;

import org.eclipse.swt.graphics.Point;

public interface CellListener {
	void notify(Point cell);
}
